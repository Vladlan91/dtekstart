package com.db.leansoft.kpi;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.db.leansoft.R;
import com.db.leansoft.kpi.dummy.UserContent;


public class ItemDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";


    private UserContent.User mItem;

    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {

            mItem = UserContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.name);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.namePosition)).setText(mItem.namePosition);
            ((TextView) rootView.findViewById(R.id.departmen)).setText(mItem.departmen);
            ((TextView) rootView.findViewById(R.id.headFirst)).setText(mItem.headFirst);
            ((TextView) rootView.findViewById(R.id.mob)).setText(mItem.mob);
            ((TextView) rootView.findViewById(R.id.phons)).setText(mItem.phons);
            ((TextView) rootView.findViewById(R.id.fax)).setText(mItem.fax);
            ((TextView) rootView.findViewById(R.id.dateOfBirth)).setText(mItem.dateOfBirth);
            ((TextView) rootView.findViewById(R.id.education)).setText(mItem.education);
            ((TextView) rootView.findViewById(R.id.experience)).setText(mItem.experience);
        }

        return rootView;
    }
}
