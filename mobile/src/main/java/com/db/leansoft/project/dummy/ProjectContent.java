package com.db.leansoft.project.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectContent {

    public static final List<Project> ITEMS = new ArrayList<Project>();

    public static final Map<String, Project> ITEM_MAP = new HashMap<String, Project>();


    private static void addItem(Project item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    static {

        addItem(new ProjectContent.Project("1","Внедрение  электронного документооборота","Фидорин Олександр Николаевич","Бойченко Борис Владимирович","04.07.2016 г.","01.11.2016 г.","07.08.2017 г.","Эффективность инвестиций","200 267 грн.","1 101 982 грн.","xxxxxxxxxxxxx","100","100","120","Внедрена" ));
        addItem(new ProjectContent.Project("2","Внедрения 5S","Стуновский Игорь Васильевич","Ступарин Олег Дмитрович","04.07.2016 г.","11.12.2016 г.","12.09.2017 г.","Эффективность управления"              ,"2 098 678 грн.",             "2 989 287 грн."                ,"xxxxxxxxxxxxx",          "80",      "100",   "100",   "Внедрена" ));
        addItem(new ProjectContent.Project("3","Внедрение системы менеджмента качества","Ломаченко Василий Станиславович","Фидорин Олександр Николаевич","04.07.2016 г.", "04.12.2016 г.", "11.10.2017 г."  ,"Эффективность управления"              ,"345 465 грн.",            "589 876 грн."   ,"xxxxxxxxxxxxx",          "75",      "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("4","Навигационная система оповещения работников","Дорош Андрей Игоревич","Ступарин Олег Дмитрович","04.07.2016 г.","08.10.2016 г.", "03.12.2016 г."  ,"Эффективность производства"           ,"342 243 грн.",                "1 345 238 грн."               ,"xxxxxxxxxxxxx",          "100",   "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("5","Ремонт блока № 9","Курновский Борис Владимирович","Дорош Андрей Игоревич","04.07.2016 г.","19.12.2016 г.","14.04.2017 г."  ,"Эффективность производства"           ,"11 345 232 грн.",          "23 987 787 грн."                ,"xxxxxxxxxxxxx",          "100",   "100",   "120",   "Внедрена" ));
        addItem(new ProjectContent.Project("6","Ремонт ПЭН","Филипчак Николай Борисович","Бойченко Борис Владимирович","04.07.2016 г.","01.11.2016 г.","07.08.2017 г.","Эффективность производства"           ,"1 232 645 грн.",             "2 121 343 грн."                ,"xxxxxxxxxxxxx",          "80",      "100",   "100",   "Внедрена" ));
        addItem(new ProjectContent.Project("7","Внедрения СУП","Ступарин Олег Дмитрович","Бойченко Борис Владимирович","04.07.2016 г.","11.12.2016 г.", "12.09.2017 г.","Эффективность производства"           ,"200 267 грн.",                "1 101 982 грн."                ,"xxxxxxxxxxxxx",          "75",      "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("8","Ремонт ПЭН","Сомолий Андрей Игоревич","Бойченко Борис Владимирович","04.07.2016 г.","04.12.2016 г.","11.10.2017 г.","Эффективность производства"           ,"2 098 678 грн.",             "2 989 287 грн."                ,"xxxxxxxxxxxxx",          "100",   "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("9","Система управления заявками","Кравец Игорь Игоревич","Фидорин Олександр Николаевич","04.07.2016 г.","08.10.2016 г.","03.12.2016 г."  ,"Эффективность управления"              ,"345 465 грн.",                "589 876 грн."             ,"xxxxxxxxxxxxx",          "80",      "100",   "100",   "Внедрена" ));
        addItem(new ProjectContent.Project("10","Замена дымососов блока № 12","Ратушняк Андрей Васильевич","Ступарин Олег Дмитрович","04.07.2016 г.","19.12.2016 г.","14.04.2017 г."  ,"Эффективность производства"           ,"342 243 грн.",                "1 345 238 грн."             ,"xxxxxxxxxxxxx",          "75",      "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("11","Капитальный ремонт блока № 6","Саланин Андрей Станиславович","Ступарин Олег Дмитрович","04.07.2016 г.","01.11.2016 г.","07.08.2017 г."  ,"Эффективность производства"           ,"11 345 232 грн.",          "23 987 787 грн."     ,"xxxxxxxxxxxxx",          "100",   "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("12","Очистка трубных систем","Яровский Андрей Игоревич","Бойченко Борис Владимирович","04.07.2016 г.","11.12.2016 г.","12.09.2017 г."  ,"Эффективность управления"              ,"1 232 645 грн.",             "2 121 343 грн."             ,"xxxxxxxxxxxxx",          "100",   "100",   "120",   "Внедрена" ));
        addItem(new ProjectContent.Project("13","Внедрение  электронного документооборота","Кузмин Борис Владимирович","Бойченко Борис Владимирович","04.07.2016 г.","04.12.2016 г.", "11.10.2017 г."  ,"Эффективность производства"           ,"200 267 грн.",    "1 101 982 грн."               ,"xxxxxxxxxxxxx",          "80",      "100",   "100",   "Внедрена" ));
        addItem(new ProjectContent.Project("14","Транспортировка золы","Паращин Андрей  Игоревич","Бойченко Борис Владимирович","04.07.2016 г.","08.10.2016 г.","03.12.2016 г."  ,"Эффективность инвестиций"               ,"2 098 678 грн.",             "2 989 287 грн."             ,"xxxxxxxxxxxxx",          "80",      "100",   "100",   "Внедрена" ));
        addItem(new ProjectContent.Project("15","Внедрение системы менеджмента качества","Коцюба Олександр Николаевич","Ступарин Олег Дмитрович","04.07.2016 г.","19.12.2016 г.", "14.04.2017 г."  ,"Эффективность управления"              ,"345 465 грн.",                "589 876 грн."   ,"xxxxxxxxxxxxx",          "75",      "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("16","Очистка трубопроводов","Дронь  Андрей Станиславович","Фидорин Олександр Николаевич","04.07.2016 г.","01.11.2016 г.","07.08.2017 г."  ,"Эффективность производства"           ,"342 243 грн.",                "1 345 238 грн."             ,"xxxxxxxxxxxxx",          "100",   "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("17","Капитальный ремонт блока № 3","Самсонов Андрей Станиславович","Ступарин Олег Дмитрович","04.07.2016 г.","11.12.2016 г.", "12.09.2017 г."  ,"Эффективность инвестиций"               ,"11 345 232 грн.",          "23 987 787 грн."     ,"xxxxxxxxxxxxx",          "100",   "100",   "120",   "Внедрена" ));
        addItem(new ProjectContent.Project("18","Внедрение системы менеджмента качества","Колос Борис Владимирович","Ступарин Олег Дмитрович","04.07.2016 г.","04.12.2016 г.", "11.10.2017 г."  ,"Эффективность управления"              ,"1 232 645 грн.",                "2 121 343 грн."               ,"xxxxxxxxxxxxx",          "80",      "100",   "100",   "Внедрена" ));
        addItem(new ProjectContent.Project("19","Очистка трубных систем","Иваницкий Андрей Станиславович","Бойченко Борис Владимирович","14.11.2016 г.","08.10.2016 г.","07.08.2017 г." ,"Эффективность производства"           ,"200 267 грн.",                "1 101 982 грн."             ,"xxxxxxxxxxxxx",          "100",   "100",   "120",   "Внедрена" ));
        addItem(new ProjectContent.Project("20","Очистка трубопроводов","Бронич Андрей  Игоревич","Бойченко Борис Владимирович","14.11.2016 г.","19.12.2016 г.","12.09.2017 г." ,"Эффективность инвестиций"               ,"2 098 678 грн.",             "2 989 287 грн."                ,"xxxxxxxxxxxxx",          "80",      "100",   "100",   "Внедрена" ));
        addItem(new ProjectContent.Project("21","Внедрения 5S","Стуновский Андрей Васильевич","Бойченко Борис Владимирович","14.11.2016 г.","01.11.2016 г.","11.10.2017 г." ,"Эффективность управления"              ,"345 465 грн.",                "589 876 грн."                ,"xxxxxxxxxxxxx",          "75",      "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("22","Замена дымососов блока № 1","Фидорин Олександр Николаевич", "Дорош Андрей Игоревич","14.11.2016 г.","11.12.2016 г.","03.12.2016 г." ,"Эффективность производства"           ,"342 243 грн.",                "1 345 238 грн."             ,"xxxxxxxxxxxxx",          "100",   "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("23","Ремонт блока № 8","Стуновский Игорь Васильевич","Ступарин Олег Дмитрович","14.11.2016 г.","04.12.2016 г.","14.04.2017 г." ,"Эффективность инвестиций"               ,"11 345 232 грн.",          "23 987 787 грн."                ,"xxxxxxxxxxxxx",          "100",   "100",   "120",   "Внедрена" ));
        addItem(new ProjectContent.Project("24","Очистка трубных систем","Ломаченко Василий Станиславович","Ступарин Олег Дмитрович","14.11.2016 г.","11.12.2016 г.","07.08.2017 г." ,"Эффективность управления"              ,"1 232 645 грн.",             "2 121 343 грн."             ,"xxxxxxxxxxxxx",          "80",      "100",   "100",   "Внедрена" ));
        addItem(new ProjectContent.Project("25","Внедрение  электронного документооборота","Дорош Андрей Игоревич","Бойченко Борис Владимирович","14.11.2016 г.","04.12.2016 г.", "12.09.2017 г." ,"Эффективность производства"           ,"200 267 грн.",    "1 101 982 грн."               ,"xxxxxxxxxxxxx",          "75",      "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("26","Замена дымососов блока № 7","Курновский Борис Владимирович","Бойченко Борис Владимирович","14.11.2016 г.", "11.12.2016 г.","11.10.2017 г." ,"Эффективность инвестиций"               ,"2 098 678 грн.",    "2 989 287 грн."               ,"xxxxxxxxxxxxx",          "100",   "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("27","Внедрения 5S","Филипчак Николай Борисович","Бойченко Борис Владимирович","14.11.2016 г.","04.12.2016 г.","07.08.2017 г.","Эффективность управления"              ,"345 465 грн.","589 876 грн.","xxxxxxxxxxxxx","80","100","100",   "Внедрена" ));
        addItem(new ProjectContent.Project("28","Ремонт ПЭН","Ступарин Олег Игоревич","Ступарин Олег Дмитрович","11.01.2017 г.","08.02.2017 г.","12.09.2017 г.","Эффективность производства","342 243 грн." ,"1 345 238 грн." ,"xxxxxxxxxxxxx", "75", "80","80","Внедрена" ));
        addItem(new ProjectContent.Project("29","Ремонт блока № 2","Сомолий Андрей Васильевич","Ступарин Олег Дмитрович","11.01.2017 г.","28.01.2017 г.", "11.10.2017 г."  ,"Эффективность инвестиций","11 345 232 грн.   ","23 987 787 грн.","xxxxxxxxxxxxx",          "100",   "80",      "80",      "Внедрена" ));
        addItem(new ProjectContent.Project("30","Внедрения 5S","Ломаченко Василий Станиславович","Дорош Андрей Игоревич","11.01.2017 г.","17.02.2017 г.", "03.12.2016 г."  ,"Эффективность управления"              ,"1 232 645 грн.  ",              "2 121 343 грн."                ,"xxxxxxxxxxxxx",          "100",   "100",   "120",   "Внедрена" ));
        addItem(new ProjectContent.Project("31","Очистка трубных систем","Дорош Андрей Игоревич","Ступарин Олег Дмитрович", "11.01.2017 г.",                "02.02.2017 г.", "14.04.2017 г."  ,"Эффективность производства"           ,"1 232 645 грн.            ",    "2 121 343 грн."                ,"xxxxxxxxxxxxx",          "80",      "100",   "100",   "Внедрена" ));
        addItem(new ProjectContent.Project("32","Транспортировка золы","Курновский  Василий Станиславович","Ступарин Олег Дмитрович","11.01.2017 г.","03.02.2017 г.","07.08.2017 г.","Эффективность инвестиций","200 267 грн.","1 101 982 грн.","xxxxxxxxxxxxx","80","100","100","Внедрена" ));
    }

    public static class Project {
        public final String id;
        public final String projectName;
        public final String projectManager ;
        public final String initiatorOfTheProject;
        public final String dateOfExaminationAndApproval  ;
        public final String theDateOfTheBeginning ;
        public final String dateOfCompletion ;
        public final String index ;
        public final String expenses ;
        public final String economicEffect ;
        public final String description ;
        public final String goal ;
        public final String times   ;
        public final String costOf  ;
        public final String status   ;

        public Project(String id, String projectName, String projectManager, String initiatorOfTheProject, String dateOfExaminationAndApproval, String theDateOfTheBeginning, String dateOfCompletion, String index, String expenses, String economicEffect, String description, String goal, String times, String costOf, String status) {
            this.id = id;
            this.projectName = projectName;
            this.projectManager = projectManager;
            this.initiatorOfTheProject = initiatorOfTheProject;
            this.dateOfExaminationAndApproval = dateOfExaminationAndApproval;
            this.theDateOfTheBeginning = theDateOfTheBeginning;
            this.dateOfCompletion = dateOfCompletion;
            this.index = index;
            this.expenses = expenses;
            this.economicEffect = economicEffect;
            this.description = description;
            this.goal = goal;
            this.times = times;
            this.costOf = costOf;
            this.status = status;
        }

        public String getId() {
            return id;
        }

        public String getProjectName() {
            return projectName;
        }

        public String getProjectManager() {
            return projectManager;
        }

        public String getInitiatorOfTheProject() {
            return initiatorOfTheProject;
        }

        public String getDateOfExaminationAndApproval() {
            return dateOfExaminationAndApproval;
        }

        public String getThe_dateOfTheBeginning() {
            return theDateOfTheBeginning;
        }

        public String getDateOfCompletion() {
            return dateOfCompletion;
        }

        public String getIndex() {
            return index;
        }

        public String getExpenses() {
            return expenses;
        }

        public String getEconomicEffect() {
            return economicEffect;
        }

        public String getDescription() {
            return description;
        }

        public String getGoal() {
            return goal;
        }

        public String getTimes() {
            return times;
        }

        public String getCostOf() {
            return costOf;
        }

        public String getStatus() {
            return status;
        }

        @Override
        public String toString() {
            return projectName;
        }
    }
}
