package com.db.leansoft.repair.chart.card;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.support.v7.widget.CardView;

import com.db.chart.Tools;
import com.db.chart.animation.Animation;
import com.db.chart.model.BarSet;
import com.db.chart.renderer.XRenderer;
import com.db.chart.renderer.YRenderer;
import com.db.chart.view.StackBarChartView;
import com.db.leansoft.R;
import com.db.leansoft.statistic.controller.CardController;

/**
 * Created by vlad on 31/01/2017.
 */

public class StackedChartRepair extends CardController {



    private final StackBarChartView mChart;

    private final String[] mLabels =
            {"ЯHВ", "ФЕВ", "МАР", "АПР", "МАЙ", "ИЮН", "ИЮЛ", "АВГ", "СЕН","ОКТ","HОЯ","ДЕК"};
    private final float[][] mValuesOne =
            {{0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
                    {90f, 50f,  90f, 90f, 90f, 80f,  90f, 40f, 69f, 90, 80, 90},
                    {10f, 0f,  20f, 50f, 60f, 0f,  70f, 0f, 0f, 30, 0, 20f}};


    public StackedChartRepair(CardView card) {

        super(card);

        mChart = (StackBarChartView) card.findViewById(R.id.chart5);

    }


    @Override
    public void show(Runnable action) {

        super.show(action);

        Paint thresPaint = new Paint();
        thresPaint.setColor(Color.parseColor("#fffefe"));
        thresPaint.setPathEffect(new DashPathEffect(new float[] {10, 20}, 0));
        thresPaint.setStyle(Paint.Style.STROKE);
        thresPaint.setAntiAlias(true);
        thresPaint.setStrokeWidth(Tools.fromDpToPx(.75f));

        BarSet stackBarSet = new BarSet(mLabels, mValuesOne[0]);
        stackBarSet.setColor(Color.parseColor("#a1d949"));
        mChart.addData(stackBarSet);

        stackBarSet = new BarSet(mLabels, mValuesOne[1]);
        stackBarSet.setColor(Color.parseColor("#5185a5"));
        mChart.addData(stackBarSet);

        stackBarSet = new BarSet(mLabels, mValuesOne[2]);
        stackBarSet.setColor(Color.parseColor("#f40743"));
        mChart.addData(stackBarSet);

        mChart.setBarSpacing(Tools.fromDpToPx(15));
        mChart.setRoundCorners(Tools.fromDpToPx(1));

        mChart.setXAxis(false)
                .setXLabels(XRenderer.LabelPosition.OUTSIDE)
                .setYAxis(false)
                .setYLabels(YRenderer.LabelPosition.NONE)
                .setValueThreshold(89.f, 89.f, thresPaint);

        int[] order = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        mChart.show(new Animation().setOverlap(.5f, order).setEndAction(action));
    }


    @Override
    public void update() {

        super.update();

        if (firstStage) {

        } else {
            mChart.updateValues(0, mValuesOne[0]);
            mChart.updateValues(1, mValuesOne[1]);
            mChart.updateValues(2, mValuesOne[2]);
        }
        mChart.notifyDataUpdate();
    }


    @Override
    public void dismiss(Runnable action) {

        super.dismiss(action);

        int[] order = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        mChart.dismiss(new Animation().setOverlap(.5f, order).setEndAction(action));
    }

}


