package com.db.leansoft.repair.chart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.db.leansoft.R;
import com.db.leansoft.head.chart.card.LineChart;
import com.db.leansoft.repair.chart.card.LineCharOne;
import com.db.leansoft.repair.chart.card.LineCharTwo;

/**
 * Created by vlad on 31/01/2017.
 */

public class RepairPoterFragment extends Fragment{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.repair_potter_fragment,parent,false);

        (new LineCharOne((CardView) layout.findViewById(R.id.card1), getContext())).init();
        (new LineCharTwo((CardView) layout.findViewById(R.id.card2), getContext())).init();
        return layout;
    }
}

