package com.db.leansoft.repair.chart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.db.leansoft.R;
import com.db.leansoft.repair.chart.card.StackedChartRepair;

/**
 * Created by vlad on 31/01/2017.
 */

public class RepairCardPotterFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.repair_card_potter_fragment,parent,false);

        (new StackedChartRepair((CardView) layout.findViewById(R.id.card5))).init();
        return layout;
    }
}

