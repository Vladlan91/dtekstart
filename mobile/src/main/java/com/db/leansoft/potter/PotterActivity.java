package com.db.leansoft.potter;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;


import com.db.leansoft.MainActivity;
import com.db.leansoft.R;

import com.db.leansoft.core.CorActivity;


public class PotterActivity extends CorActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_potter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}