package com.db.leansoft.potter.chart.card;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.widget.TextView;

import com.db.chart.Tools;
import com.db.chart.animation.Animation;
import com.db.chart.listener.OnEntryClickListener;
import com.db.chart.model.BarSet;
import com.db.chart.renderer.XRenderer;
import com.db.chart.renderer.YRenderer;
import com.db.chart.view.StackBarChartView;
import com.db.leansoft.R;
import com.db.leansoft.statistic.controller.CardController;


public class StackedChartPotter extends CardController {


	private final StackBarChartView mChart;

	private final String[] mLabels =
			  {"2004", "2005", "2006", "2008", "2009", "2010", "2011", "2012", "2013","2014","2015","2016"};
	private final float[][] mValuesOne =
			            {{0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
						 {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f},
						 {5f, 5f,  10f, 11f, 15f, 20f,  35f, 40f, 69f, 72, 80, 91f}};


	public StackedChartPotter(CardView card) {

		super(card);

		mChart = (StackBarChartView) card.findViewById(R.id.chart5);

	}


	@Override
	public void show(Runnable action) {

		super.show(action);

		Paint thresPaint = new Paint();
		thresPaint.setColor(Color.parseColor("#fffefe"));
		thresPaint.setPathEffect(new DashPathEffect(new float[] {10, 20}, 0));
		thresPaint.setStyle(Paint.Style.STROKE);
		thresPaint.setAntiAlias(true);
		thresPaint.setStrokeWidth(Tools.fromDpToPx(.75f));

		BarSet stackBarSet = new BarSet(mLabels, mValuesOne[0]);
		stackBarSet.setColor(Color.parseColor("#a1d949"));
		mChart.addData(stackBarSet);

		stackBarSet = new BarSet(mLabels, mValuesOne[1]);
		stackBarSet.setColor(Color.parseColor("#f4de07"));
		mChart.addData(stackBarSet);

		stackBarSet = new BarSet(mLabels, mValuesOne[2]);
		stackBarSet.setColor(Color.parseColor("#f63740"));
		mChart.addData(stackBarSet);

		mChart.setBarSpacing(Tools.fromDpToPx(15));
		mChart.setRoundCorners(Tools.fromDpToPx(1));

		mChart.setXAxis(false)
				  .setXLabels(XRenderer.LabelPosition.OUTSIDE)
				  .setYAxis(false)
				  .setYLabels(YRenderer.LabelPosition.NONE)
				  .setValueThreshold(89.f, 89.f, thresPaint);

		int[] order = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
		mChart.show(new Animation().setOverlap(.5f, order).setEndAction(action));
	}


	@Override
	public void update() {

		super.update();

		if (firstStage) {

		} else {
			mChart.updateValues(0, mValuesOne[0]);
			mChart.updateValues(1, mValuesOne[1]);
			mChart.updateValues(2, mValuesOne[2]);
		}
		mChart.notifyDataUpdate();
	}


	@Override
	public void dismiss(Runnable action) {

		super.dismiss(action);

		int[] order = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
		mChart.dismiss(new Animation().setOverlap(.5f, order).setEndAction(action));
	}

}
