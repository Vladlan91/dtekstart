package com.db.leansoft.potter.chart;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.db.leansoft.R;
import com.db.leansoft.potter.chart.card.StackedChartPotter;


/**
 * Created by vlad on 29/01/2017.
 */

public class PoterCardFragment extends android.support.v4.app.Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.potter_chart_fragment,parent,false);

        (new StackedChartPotter((CardView) layout.findViewById(R.id.card5))).init();
        return layout;
    }
}
