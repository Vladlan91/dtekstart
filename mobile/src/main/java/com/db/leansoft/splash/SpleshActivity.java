package com.db.leansoft.splash;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.db.leansoft.MainActivity;
import com.db.leansoft.R;

public class SpleshActivity extends AppCompatActivity {

    private ImageView tv;
    private TextView iv;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        tv = (ImageView) findViewById(R.id.start);
        iv = (TextView) findViewById(R.id.textstart);
        android.view.animation.Animation myanim = AnimationUtils.loadAnimation(this,R.anim.mytransition);
        android.view.animation.Animation myanim2 = AnimationUtils.loadAnimation(this,R.anim.transle);
        tv.startAnimation(myanim);
        iv.startAnimation(myanim2);


        Thread myThread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(6000);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();
    }
}