package com.db.leansoft.statistic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.db.leansoft.MainActivity;
import com.db.leansoft.R;
import com.db.leansoft.statistic.controller.ChartsFragment;


public class StatisticActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


	private android.support.v4.app.Fragment mCurrFragment;

	private int currSpinnerSelection = 0;

	private ArrayAdapter<CharSequence> mAdapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		mAdapter = ArrayAdapter.createFromResource(this, R.array.spinner_options,
				  android.R.layout.simple_spinner_item);
		mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		switch (currSpinnerSelection){
			case 0: mCurrFragment = new ChartsFragment(); break;

		}

		getSupportFragmentManager().beginTransaction().add(R.id.container, mCurrFragment).commit();
	}


	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {}

	@Override
	public void onBackPressed() {
		startActivity(new Intent(this, MainActivity.class));
		finish();
	}

}
