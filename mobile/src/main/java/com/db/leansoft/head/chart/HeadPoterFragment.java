package com.db.leansoft.head.chart;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v7.widget.CardView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



import com.db.leansoft.R;
import com.db.leansoft.head.chart.card.LineChart;


public class HeadPoterFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.head_potter_fragment,parent,false);

        (new LineChart((CardView) layout.findViewById(R.id.card1), getContext())).init();
        return layout;
    }
}
