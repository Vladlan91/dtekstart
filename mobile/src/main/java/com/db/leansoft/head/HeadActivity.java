package com.db.leansoft.head;


import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import com.db.leansoft.MainActivity;
import com.db.leansoft.R;
import com.db.leansoft.head.slider.project.ProjectFragmentSlider;
import com.db.leansoft.head.slider.user.UserFragmentSlider;


public class HeadActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.head_list_activity);

        FragmentManager user = getSupportFragmentManager();
        Fragment fragment = user.findFragmentById(R.id.user_list_id);
        if (fragment == null) {
            fragment = new UserFragmentSlider();
            user.beginTransaction()
                    .add(R.id.user_list_id, fragment)
                    .commit();
        }
            FragmentManager project = getSupportFragmentManager();
            Fragment fragment2 = project.findFragmentById(R.id.project_list_id);
            if (fragment2 == null) {
                fragment2 = new ProjectFragmentSlider();
                project.beginTransaction()
                        .add(R.id.project_list_id, fragment2)
                        .commit();
            }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}

