#LeanSoft ![tablet][7]

Проект складається з Navigation drawer, Master/Detail flow, Setting activity i головного фрагмента MainFragment.

На данний момент налаштовано:

* ``SpleshActivity   `` 
Стартова активність, також підключена анімація, яка знаходиться в папці :anym
* ``StatisticActivity`` 
Підключено і налаштовано графіки бібліотека знаходиться в модулі williamchart, дані графіки необхідно адаптувати під наш проект. 
* ``HeadActivity папка head``
Активність яка міститиме в собі загальну інформацію про виробництво 
* ``IntroActivity папка aboutProject``
Презентаційна частина продукту LeanSoft ![tablet][7]
* ``SettingsActivity папка settings``
Активність налаштувань проекту
* ``ItemListActivity папка kpi```
Активність ключових показників ефективності з використанням бібліотеки android - Master/Detail flow
* ``ProjectListActivity папка project``
Активність Cистеми управління проектами з використанням бібліотеки android - Master/Detail flow

![gif1][1]

![gif1][2]

Основні налаштування
--------------------

MinSdkVersion 24 ![tablet][8]

###Gradle library
``` groovy
    compile 'com.android.support:appcompat-v7:24.2.1'
    compile 'com.android.support:design:24.2.1'
    compile 'com.android.support:cardview-v7:24.2.1'
    compile 'com.larswerkman:HoloColorPicker:1.5'
    compile 'com.android.support:support-v4:24.2.1'
    compile 'com.google.android.gms:play-services:10.0.1'
    compile 'com.android.support.constraint:constraint-layout:1.0.0-alpha7'
    compile 'com.android.support:recyclerview-v7:24.2.1'
    compile 'com.android.support:support-vector-drawable:24.2.1'
```




[1]: ./art/2.3.0-1.gif
[2]: ./art/2.3.0-2.gif
[7]: ./art/phone.png
[8]: ./art/watch.png